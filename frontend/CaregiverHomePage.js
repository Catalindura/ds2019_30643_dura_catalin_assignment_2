import React from "react"
import CaregiverNavBar from "./CaregiverNavBar"
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import axios from "axios"

class CaregiverHomePage extends React.Component{
    constructor(){
        super()
        this.state={
            activities:[],
        }
        this.initializeWebSocket=this.initializeWebSocket.bind(this)
}

componentDidMount(){
    this.initializeWebSocket()
}

initializeWebSocket(){
    let ws = new SockJS('http://localhost:8090/socket');
    let stompClient = Stomp.over(ws);
    let act=[];
    const _this=this
    stompClient.connect({},(frame)=> {
        stompClient.subscribe("/queue",(Event)=>{
            act.push(JSON.parse(Event.body))
            this.setState({
                activities:act
            })
            console.log(act)
        }) 
    },null);
    axios.get("http://localhost:8090/send")
    .then(response=>{
            console.log(response.data)
    })
}

    render(){
        return(
            <div>
                <CaregiverNavBar/>
                <table>
                    <tbody>
                        <tr>
                            <td>Invalid Activity</td>
                            <td>Start Time</td>
                            <td>End Time</td>
                        </tr>
                        {this.state.activities.map(activity=>
                            <tr>
                              <td>{activity.name}</td>
                              <td>{activity.startTime}</td>
                              <td>{activity.endTime}</td>
                            </tr>)}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default CaregiverHomePage