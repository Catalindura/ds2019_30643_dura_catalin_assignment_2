package Controller;

import Entity.Activity;
import Rabbit.Producer;
import Service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin(maxAge = 3600)
public class RabbitController {
    @Autowired
    Producer producer;
    @Autowired
    ActivityService activityService;

    @Transactional
    @GetMapping("/send")
    public String sendMessage() throws IOException {
        activityService.deleteAll();
        producer.produceMsg();
        return "Successfully Msg sent";
    }
}
