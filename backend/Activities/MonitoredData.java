package Activities;

import java.io.Serializable;
import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;

public class MonitoredData implements Serializable {
    private DateTime startTime;
    private DateTime endTime;
    private String name;



    public MonitoredData(DateTime st,DateTime et,String n) {
        startTime=st;
        endTime=et;
        name=n;
    }
    public MonitoredData() {}

    public int getZi() {
        return startTime.getDayOfMonth();
    }

    public String getStartTimeString(){
        return startTime.getYear()+"-"+startTime.getMonthOfYear()+"-"+startTime.getDayOfMonth()+"-"+startTime.getHourOfDay()+
                "-"+startTime.getMinuteOfHour()+"-"+startTime.getSecondOfMinute();
    }

    public String getEndTimeString(){
        return endTime.getYear()+"-"+endTime.getMonthOfYear()+"-"+endTime.getDayOfMonth()+"-"+endTime.getHourOfDay()+
                "-"+endTime.getMinuteOfHour()+"-"+endTime.getSecondOfMinute();
    }

    public String toString() {
        return name+" Start Time:"+startTime+ "End Time:"+endTime;
    }
    public String getName() {
        return name;
    }
    public DateTime getStartTime() {
        return startTime;
    }
    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }
    public DateTime getEndTime() {
        return endTime;
    }
    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }
    public void setName(String name) {
        this.name = name;
    }

}