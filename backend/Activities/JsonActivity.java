package Activities;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class JsonActivity {
    public String name;
    public String startTime;
    public String endTime;

    public JsonActivity(){

    }

    public JsonActivity(String name, String startTime, String endTime) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public long validActivity(){
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
        DateTime myDate1 = dtf.parseDateTime(this.startTime);
        DateTime myDate2= dtf.parseDateTime(this.endTime);
        Date inceput=myDate1.toDate();
        Date sfarsit=myDate2.toDate();
        long result = sfarsit.getTime() - inceput.getTime();
        TimeUnit timeUnit=TimeUnit.MINUTES;
        long difference=timeUnit.convert(result,TimeUnit.MILLISECONDS);
        return difference;
    }
    @Override
    public String toString() {
        return "JsonActivity{" +
                "name='" + name + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }
}
