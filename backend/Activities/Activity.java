package Activities;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Activity {

    public List<MonitoredData> getData() throws IOException{
        List<MonitoredData> data=new ArrayList<MonitoredData>();
        List<String> lista=new ArrayList<String>();
        String fileName = "activity.txt";

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            lista=stream.collect(Collectors.toList());
            data=lista
                    .stream()
                    .map(x->{
                        String[] vect=x.split("\\s+");
                        DateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
                        String date1=vect[0]+" "+vect[1];
                        String date2=vect[2]+" "+vect[3];
                        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                        DateTime myDate1 = dtf.parseDateTime(date1);
                        DateTime myDate2 = dtf.parseDateTime(date2);
                        MonitoredData dataa=new MonitoredData(myDate1,myDate2,vect[4]);
                        return dataa;
                    })
                    .collect(Collectors.toList());
            return data;
        }
    }
}
