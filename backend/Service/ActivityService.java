package Service;

import Entity.Activity;
import Repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService {
    @Autowired
    ActivityRepository activityRepository;

    public void saveActivity(Activity activity){
        activityRepository.save(activity);
    }

    public void deleteAll(){
        activityRepository.deleteAllByPatientIdPatient(31);
    }
}
