package Entity;


import org.springframework.context.annotation.Configuration;

import javax.persistence.*;

@Entity
@Table(name="activity")
public class Activity {
    @Id
    @Column(name="idactivity",nullable=false,unique=true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer idActivity;

    @ManyToOne
    @JoinColumn(name="idpatient")
    private Patient patient;

    @Column(name="name")
    private String name;

    @Column(name="starttime")
    private String startTime;

    @Column(name="endtime")
    private String endTime;

    public Activity(){

    }

    public Activity(Patient patient, String name, String startTime, String endTime) {
        this.patient = patient;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(Integer idActivity) {
        this.idActivity = idActivity;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
