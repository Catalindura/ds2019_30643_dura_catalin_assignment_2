package Repository;

import Entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityRepository extends JpaRepository<Activity,Integer> {
    public Activity save(Activity activity);
    public void deleteAllByPatientIdPatient(int id);
}
