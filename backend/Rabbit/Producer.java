package Rabbit;

import Activities.Activity;
import Activities.JsonActivity;
import Activities.MonitoredData;
import Dto.PatientDto;
import Entity.Patient;
import Service.ActivityService;
import Service.PatientService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
public class Producer {
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private ActivityService activityService;

    @Autowired
    PatientService patientService;

    @Value("${rabbitmq.queue}")
    private String queueName;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingKey;


    public void produceMsg() throws IOException {
        Activity activity=new Activity();
        int nr=0;
        PatientDto patientDto=patientService.getPatientById(31);
        Patient patient=patientDto.patientDtoToPatient();
        for(MonitoredData aux:activity.getData()) {
            try {
                nr = nr + 1;
                System.out.println(nr);
                if (nr % 10==0) Thread.sleep(1000);
                JsonActivity jsonActivity = new JsonActivity(aux.getName(), aux.getStartTimeString(), aux.getEndTimeString());
                activityService.saveActivity(new Entity.Activity(patient,aux.getName(),aux.getStartTimeString(),aux.getEndTimeString()));
                amqpTemplate.convertAndSend(exchange, routingKey, jsonActivity);
            }
            catch(InterruptedException ie){
                Thread.currentThread().interrupt();
            }
        }
    }
}
