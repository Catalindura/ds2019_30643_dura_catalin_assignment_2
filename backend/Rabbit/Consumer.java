package Rabbit;

import Activities.JsonActivity;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private SimpMessagingTemplate messageSender;

    @RabbitListener(queues="${rabbitmq.queue}",containerFactory = "jsaFactory")
    public void onMessage(JsonActivity jsonActivity){
        long difference=jsonActivity.validActivity();
        if(jsonActivity.getName().equals("Sleeping")) {
            if (difference > 600) {
               System.out.println(jsonActivity.toString());
                messageSender.convertAndSend("/queue", jsonActivity);
            }
        }
        if(jsonActivity.getName().equals("Leaving")) {
            if (difference > 120) {
                System.out.println(jsonActivity.toString());
                messageSender.convertAndSend("/queue", jsonActivity);
            }
        }

        if(jsonActivity.getName().equals("Toileting")) {
            if (difference > 5) {
                System.out.println(jsonActivity.toString());
                messageSender.convertAndSend("/queue", jsonActivity);
            }
        }

    }
}
